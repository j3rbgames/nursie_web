var app = new Vue({
    el: '#vueApp',
    // storing the state of the page
    data: {
        connected: false,
        ros: null,
        logs: [],
        loading: false,
        service_busy: false,
        service_response: '',
        rosbridge_address: 'ws://127.0.0.1:9090',
        port: '9090',
        param_val: 0,
        goal: null,
        action: {
            goal: {
                position: {
                    x: 0, y: 0, z: 0
                }
            },
            feedback: {
                position: {
                    x: 0, y: 0
                },
                state: ''
            },
            result: {
                success: false
            },
            status: {
                status: 0,
                text: ''
            },
        },
        initialPose: {
            seq: 0,
            position: {
                x: -3, y: 1
            }
        }
    },

    // helper methods to connect to ROS
    methods: {
        connect: function () {
            this.loading = true
            this.ros = new ROSLIB.Ros({
                url: this.rosbridge_address
            })
            this.ros.on('connection', () => {
                this.logs.unshift((new Date()).toTimeString() + ' - Connected!')
                this.connected = true
                this.loading = false
            })
            this.ros.on('error', (error) => {
                this.logs.unshift((new Date()).toTimeString() + ` - Error: ${error}`)
            })
            this.ros.on('close', () => {
                this.logs.unshift((new Date()).toTimeString() + ' - Disconnected!')
                this.connected = false
                this.loading = false
            })
        },

        disconnect: function () {
            this.ros.close()
        },

        sendInitialPose: function () {
            let topic = new ROSLIB.Topic({
                ros: this.ros,
                name: '/initialpose',
                messageType: 'geometry_msgs/PoseWithCovarianceStamped'
            })
            let message = new ROSLIB.Message({
                header: {
                    seq: this.initialPose.seq,
                    stamp: null,
                    frame_id: 'map'
                },
                pose: {
                    pose: {
                        position: {
                            x: this.initialPose.position.x, y: this.initialPose.position.y, z: 0
                        },
                        orientation: {
                            x: 0, y: 0, z: 0, w: 1
                        }
                    },
                    covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                }
            })
            topic.publish(message)
            this.initialPose.seq += 1
        },

        sendGoal: function () {
            let actionClient = new ROSLIB.ActionClient({
                ros: this.ros,
                serverName: '/move_base',
                actionName: 'move_base_msgs/MoveBaseAction'
            })

            this.goal = new ROSLIB.Goal({
                actionClient: actionClient,
                goalMessage: {
                    target_pose: {
                        header: {
                            frame_id: 'map'
                        },
                        pose: {
                            position: this.action.goal.position,
                            orientation: { x: 0, y: 0, z: 0, w: 1 }
                        }
                    },
                }
            })

            this.goal.on('status', (status) => {
                this.action.status = status

            })

            this.goal.on('feedback', (feedback) => {
                this.action.feedback = feedback.base_position.pose

            })



            this.goal.send()
        },

        cancelGoal: function () {
            this.goal.cancel()
        },

        callMoveTo: function () {
            this.service_busy = true
            this.service_response = ''

            // define the service to call
            let service = new ROSLIB.Service({
                ros: this.ros,
                name: '/nursie_navigation_fsm',
                serviceType: 'nursie_msg/Ubicacion_Msg'
            })

            // define the request
            let request = new ROSLIB.ServiceRequest({
                ubicacion: this.param_val
            })

            // define the callback
            service.callService(request, (result) => {
                this.service_busy = false
                this.service_response = JSON.stringify(result)
            }, (error) => {
                this.service_busy = false
                console.error(error)
            })
        }

    },
    mounted() {
    },
})
